module Codewars.Kata.DNA where
import Codewars.Kata.DNA.Types

-- data Base = A | T | G | C
type DNA = [Base]

complement :: Base -> Base
complement base = case base of
    A -> T
    T -> A
    G -> C
    C -> G

dnaStrand :: DNA -> DNA
dnaStrand [] = []
dnaStrand dnaArray = map complement dnaArray