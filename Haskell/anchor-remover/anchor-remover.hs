module RemoveUrlAnchor where

import Data.Text

removeUrlAnchor :: String -> String
removeUrlAnchor url = do
  case (findIndex(=='#') (pack url)) of
    Just n -> unpack (Data.Text.take n (pack url))
    Nothing -> url