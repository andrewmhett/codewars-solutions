module Codewars.Kata.Bus where

number :: [(Int, Int)] -> Int
number [] = 0
number ((i,o):n) = (i-o) + number n