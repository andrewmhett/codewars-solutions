let swap (c: char): char =
  if c == 'a' then
    'b'
  else if c == 'b' then
    'a'
  else
    'c'

let switcheroo (s: string): string =
  Base.String.of_char_list (List.map swap (Base.String.to_list s))