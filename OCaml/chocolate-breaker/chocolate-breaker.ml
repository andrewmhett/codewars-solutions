let break_chocolate (n: int) (m: int): int =
  if n * m > 0 then (n * m) - 1 else 0