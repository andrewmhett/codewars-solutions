let closest_multiple_10 (i: int): int =
  let rounded = if i mod 10 >= 5 then
    ceil (float_of_int i /. 10.)
  else
    floor (float_of_int i /. 10.)
  in 10 * int_of_float rounded