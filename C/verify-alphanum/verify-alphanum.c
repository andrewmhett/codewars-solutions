#include <stdbool.h>

bool alphanumeric(const char* strin) {
  if (strlen(strin) == 0) return false;
  bool is_alphanumeric = true;
  for (int i = 0; i < strlen(strin); i++) {
    if ((strin[i] < '0' || strin[i] > '9') && (strin[i] < 'a' || strin[i] > 'z')
       && (strin[i] < 'A' || strin[i] > 'Z')) {
      is_alphanumeric = false;
      break;
    }
  }
  return is_alphanumeric;
}