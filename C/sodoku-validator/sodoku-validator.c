#include<stdbool.h>

bool validSolution(unsigned int board[9][9]){
  for (int number = 1; number <= 9; number++) {
    bool contains_num = false;
    for (int row = 0; row < 9; row++) {
      contains_num = false;
      for (int col = 0; col < 9; col++) {
        if (board[row][col] == number) {
          contains_num = true;
          break;
        }
      }
      if (!contains_num) return false;
    }
    for (int col = 0; col < 9 && !contains_num; col++) {
      contains_num = false;
      for (int row = 0; row < 9; row++) {
        if (board[row][col] == number) {
          contains_num = true;
          break;
        }
      }
      if (!contains_num) return false;
    }
    for (int base_col = 0; base_col < 9; base_col += 3) {
      for (int base_row = 0; base_row < 9; base_row += 3) {
        contains_num = false;
        for (int col = base_col; col < base_col + 3 && !contains_num; col++) {
          for (int row = base_row; row < base_row + 3; row++) {
            if (board[row][col] == number) {
              contains_num = true;
              break;
            }
          }
        }
        if (!contains_num) return false;
      }
    }
  }
  return true;
}