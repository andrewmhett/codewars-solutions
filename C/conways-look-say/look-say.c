#include <stdio.h>
#include <string.h>
#include <math.h>

typedef unsigned long long ullong;

#define IN_BUF_SIZE 20
#define OUT_BUF_SIZE 40

ullong look_say (ullong n)
{
  char in_string[IN_BUF_SIZE];
  char out_string[OUT_BUF_SIZE];
  sprintf(&in_string[0], "%lld", n);
  char last_char = in_string[0];
  int last_index = 0;
  char* out_ptr = &out_string[0];
  for (int i = 0; i < strlen(in_string); i++) {
    if (in_string[i] == last_char) continue;
    out_ptr += sprintf(out_ptr, "%d", i - last_index);
    *out_ptr = last_char;
    out_ptr += 1;
    last_char = in_string[i];
    last_index = i;
  }
  out_ptr += sprintf(out_ptr, "%ld", strlen(in_string) - last_index);
  *out_ptr = last_char;
  out_ptr += 1;
  *out_ptr = 0;
  ullong out_val = 0;
  for (int i = 0; i < strlen(out_string); i++) {
    out_val += (ullong) pow(10, i) * (out_string[strlen(out_string)-1-i] - '0');
  }
  return out_val;
}
