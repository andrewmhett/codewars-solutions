#include <stddef.h>
#include <stdio.h>

int maxSequence(const int* array, size_t n) {
  printf("\n");
  if (n == 0) return 0;
  int all_negative = 1;
  for (size_t i = 0; i < n; i++) {
    if (array[i] >= 0) {
      all_negative = 0;
      break;
    }
  }
  int greatest = 0;
  int sum;
  if (all_negative) return 0;
  for (size_t s = 0; s < n; s++) {
    for (size_t e = s+1; e < n; e++) {
      sum = 0;
      for (size_t o = s; o <= e; o++) {
        sum += array[o];
      }
      if (sum > greatest) greatest = sum;
    }
  }
  return greatest;
}