#include <math.h>

// Returns number of complete beeramid levels
int beeramid(double bonus, double price) {
  if (bonus <= 0) return 0;
  int max_levels = 0;
  int level = 1;
  while (bonus > 0) {
    bonus -= (level * level * price);
    level++;
  }
  if (bonus == 0) return level - 1;
  return level - 2;
}