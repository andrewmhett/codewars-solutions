#include <stddef.h>
#include <stdio.h>

/* Do not allocate any memory but just use rdata and return it back */

unsigned char *data_reverse(unsigned char *rdata, const unsigned char *data, size_t nblk)
{
  size_t current_bit = 0;
  for (int byte_start = (nblk-1)*8; byte_start >= 0; byte_start -= 8) {
    for (int bit_position = byte_start; bit_position < byte_start+8; bit_position++) {
      rdata[current_bit] = data[bit_position];
      current_bit++;
    }
  }
  return rdata;
}