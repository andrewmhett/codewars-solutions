char *rot13 (char *str_out, const char *str_in)
{
  for (int i = 0; i < strlen(str_in); i++) {
    char new_char = str_in[i] - 13;
    if (str_in[i] >= 65 && str_in[i] <= 90) {
      if (new_char < 65) new_char = 91 - (65 - new_char);
    } else if (str_in[i] >= 97 && str_in[i] <= 122) {
      if (new_char < 97) new_char = 123 - (97 - new_char);
    } else {
      new_char += 13;
    }
    str_out[i] = new_char;
  }
  str_out[strlen(str_in)] = 0;
  return str_out;
}