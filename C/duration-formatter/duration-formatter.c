#include <stdlib.h>
#include <string.h>
#include <math.h>

int int_to_string(char* out_buf, int value) {
  int char_position = 0;
  if (value < 0) {
    out_buf[char_position] = '-';
    value *= -1;
    char_position++;
  } else if (value == 0) {
    out_buf[0] = '0';
    return 1;
  }
  int num_digits = floor(log10(value)) + 1;
  char value_chars[num_digits];
  for (int i = 1; i <= num_digits; i++) {
    value_chars[num_digits-i] = (char)((value % (int)pow(10, i)) / (int)pow(10, i-1) + '0');
    value -= value % (int)pow(10, i);
  }
  for (int i = 0; i < num_digits; i++) {
    out_buf[char_position] = value_chars[i];
    char_position++;
  }
  return char_position;
}

int time_to_string(char* out_buf, const char* unit, int n) {
  int char_position = 0;
  char_position += int_to_string(out_buf, n);
  strcpy(&out_buf[char_position], " ");
  char_position++;
  strcpy(&out_buf[char_position], unit);
  char_position += strlen(unit);
  if (n != 1) {
    strcpy(&out_buf[char_position], "s");
    char_position++;
  }
  return char_position;
}

char *formatDuration (int n) {
  int char_position = 0;
  char* out_string = malloc(50);
  if (n == 0) {
    strcpy(out_string, "now");
    char_position = 3;
  } else {
    int years = 0, days = 0, hours = 0, minutes = 0, seconds = 0;
    int has_days = 0, has_hours = 0, has_minutes = 0, has_seconds = 0;
    years = n / 31536000;
    n %= 31536000;
    days = n / 86400;
    if (days != 0) has_days = 1;
    n %= 86400;
    hours = n / 3600;
    if (hours != 0) has_hours = 1;
    n %= 3600;
    minutes = n / 60;
    if (minutes != 0) has_minutes = 1;
    n %= 60;
    seconds = n;
    if (seconds != 0) has_seconds = 1;
    if (years != 0) {
      char_position += time_to_string(&out_string[char_position], "year", years);
      if (has_days + has_hours + has_minutes + has_seconds > 1) {
        strcpy(&out_string[char_position], ", ");
        char_position += 2;
      } else if (has_days + has_hours + has_minutes + has_seconds == 1) {
        strcpy(&out_string[char_position], " and ");
        char_position += 5;
      }
    }
    if (days != 0) {
      char_position += time_to_string(&out_string[char_position], "day", days);
      if (has_hours + has_minutes + has_seconds > 1) {
        strcpy(&out_string[char_position], ", ");
        char_position += 2;
      } else if (has_hours + has_minutes + has_seconds == 1) {
        strcpy(&out_string[char_position], " and ");
        char_position += 5;
      }
    }
    if (hours != 0) {
      char_position += time_to_string(&out_string[char_position], "hour", hours);
      if (has_minutes + has_seconds > 1) {
        strcpy(&out_string[char_position], ", ");
        char_position += 2;
      } else if (has_minutes + has_seconds == 1) {
        strcpy(&out_string[char_position], " and ");
        char_position += 5;
      }
    }
    if (minutes != 0) {
      char_position += time_to_string(&out_string[char_position], "minute", minutes);
      if (has_seconds) {
        strcpy(&out_string[char_position], " and ");
        char_position += 5;
      }
    }
    if (seconds != 0) char_position += time_to_string(&out_string[char_position], "second", seconds);
  }
  out_string[char_position] = 0;
  return out_string;
}