#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

char *camel_case(const char *s)
{
  int counter=0;
  int capital=1;
  int size=0;
  while (s[size+1] != '\n'){
    size++;
  }
  char *out=(char*)malloc(size*sizeof(char));
  for (int i=0;i<size;i++){
    if (s[i] != ' '){
      if (capital){
        out[counter]=toupper(s[i]);
        counter++;
        capital=0;
      }else{
        out[counter]=s[i];
        counter++;
      }
    }else{
      capital=1;
    }
  }
  return out;
}