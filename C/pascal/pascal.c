unsigned get_flattened_idx(unsigned row, unsigned col) {
  return (unsigned) (row * ((float) row + 1) / 2) + col;
}

void pascals_triangle (unsigned n, unsigned triangle[n * (n + 1) / 2]) {
  unsigned flattened_idx = 0;
  for (unsigned row = 0; row < n; row++) {
    for (unsigned col = 0; col <= row; col++) {
      if (col == 0 || col == row) {
        triangle[flattened_idx++] = 1;
        continue;
      }
      unsigned left_parent = triangle[get_flattened_idx(row - 1, col - 1)];
      unsigned right_parent = triangle[get_flattened_idx(row - 1, col)];
      triangle[flattened_idx++] = left_parent + right_parent;
    }
  }
}
