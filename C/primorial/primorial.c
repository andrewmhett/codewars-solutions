#include <math.h>

int is_prime(int n) {
  int return_bool = 1;
  for (int denominator = 2; denominator <= sqrt(n); denominator++) {
    if (!(n % denominator)) {
      return_bool = 0;
      break;
    }
  }
  return return_bool;
}

unsigned long long num_primorial(unsigned n) {
  unsigned long primorial = 1;
  unsigned int num_primes = 0;
  int i = 1;
  while (num_primes < n) {
    i++;
    if (is_prime(i)) {
      primorial *= i;
      num_primes++;
    }
  }
  return primorial;
}