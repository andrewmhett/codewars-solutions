#include <stdlib.h>
#include <math.h>

int int_to_string(char* out_buf, int value) {
  int char_position = 0;
  if (value < 0) {
    out_buf[char_position] = '-';
    value *= -1;
    char_position++;
  } else if (value == 0) {
    out_buf[0] = '0';
    return 1;
  }
  int num_digits = floor(log10(value)) + 1;
  char value_chars[num_digits];
  for (int i = 1; i <= num_digits; i++) {
    value_chars[num_digits-i] = (char)((value % (int)pow(10, i)) / (int)pow(10, i-1) + '0');
    value -= value % (int)pow(10, i);
  }
  for (int i = 0; i < num_digits; i++) {
    out_buf[char_position] = value_chars[i];
    char_position++;
  }
  return char_position;
}

char *range_extraction(const int *args, size_t n)
{
  char* output_string = malloc(4*n);
  int char_position = 0;
  for (int i = 0; i < n; i++) {
    if (i == 0) {
      char_position += int_to_string(&output_string[char_position], args[i]);
      continue;
    }
    if (args[i] - args[i-1] > 1 || args[i+1] - args[i] != 1) {
      output_string[char_position] = ',';
      char_position++;
      char_position += int_to_string(&output_string[char_position], args[i]);
    } else {
      output_string[char_position] = '-';
      char_position++;
      while (args[i] - args[i-1] == 1) i++;
      i--;
      char_position += int_to_string(&output_string[char_position], args[i]);
    }
  }
  output_string[char_position] = 0;
  return output_string;
}