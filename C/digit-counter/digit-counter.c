#include <stdio.h>

int nbDig(int n, int d) {
  unsigned long long squares[n+1];
  int counter=0;
  for (int i=0;i<=n;i++){
    squares[i]=(unsigned long long)(i*i);
  }
  for (int i=0;i<n+1;i++){
    char buf[20];
    int len=sprintf(buf,"%llu",squares[i]);
    for (int o=0;o<len;o++){
      if (buf[o]-'0'==d){
        counter++;
      }
    }
  }
  return counter;
}