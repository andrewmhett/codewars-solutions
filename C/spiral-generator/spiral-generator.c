#include <math.h>
#include <string.h>

void spiralize (unsigned n, int spiral[n][n]) {
  int x = 0, y = 0;
  int side_length = n;
  int iterations = 0;
  memset(spiral, 0, n*n*sizeof(int));
  while (iterations <= n) {
    for (; x < side_length; x++) {
      spiral[y][x] = 1;
    }
    x--;
    if (++iterations == n) {
      spiral[y][x] = 1;
      break;
    }
    for (; y < side_length; y++) {
      spiral[y][x] = 1;
    }
    y--;
    if (++iterations == n) {
      spiral[y][x] = 1;
      break;
    }
    for (; x > n - side_length; x--) {
      spiral[y][x] = 1;
    }
    if (++iterations == n) {
      spiral[y][x] = 1;
      break;
    }
    side_length -= 2;
    for (; y > n - side_length; y--) {
      spiral[y][x] = 1;
    }
    if (++iterations == n) {
      spiral[y][x] = 1;
      break;
    }
  }
}