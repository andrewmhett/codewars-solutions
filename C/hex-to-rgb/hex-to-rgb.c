typedef struct {
    int r, g, b;
} rgb;

typedef struct {
  char hex_char;
  int value;
} hex_value_pair;

int hex_to_decimal(char hex_char, hex_value_pair* value_lookup) {
  int return_value;
  for (int i = 0; i < 22; i++) {
    if (value_lookup[i].hex_char == hex_char) return_value = value_lookup[i].value;
  }
  return return_value;
}

rgb hex_str_to_rgb(const char *hex_str) {
  rgb result;
  hex_value_pair value_lookup[22];
  int lookup_pos = 0;
  for (int i = '0'; i <= '9'; i++) {
    value_lookup[lookup_pos].hex_char = (char)i;
    value_lookup[lookup_pos].value = i - '0';
    lookup_pos++;
  }
  for (int i = 'A'; i <= 'F'; i++) {
    value_lookup[lookup_pos].hex_char = (char)i;
    value_lookup[lookup_pos].value = 10 + i - 'A';
    lookup_pos++;
  }
  for (int i = 'a'; i <= 'f'; i++) {
    value_lookup[lookup_pos].hex_char = (char)i;
    value_lookup[lookup_pos].value = 10 + i - 'a';
    lookup_pos++;
  }
  result.r = 16 * hex_to_decimal(hex_str[1], value_lookup) + hex_to_decimal(hex_str[2], value_lookup);
  result.g = 16 * hex_to_decimal(hex_str[3], value_lookup) + hex_to_decimal(hex_str[4], value_lookup);
  result.b = 16 * hex_to_decimal(hex_str[5], value_lookup) + hex_to_decimal(hex_str[6], value_lookup);
  return result;
}