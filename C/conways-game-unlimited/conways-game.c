#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *htmlize (int **, int, int);

int **get_generation (int **cells, int generations, int *rowptr, int *colptr)
{
  int** universe = malloc(sizeof(int*) * (*rowptr));
  for (int row = 0; row < *rowptr; row++) {
    universe[row] = malloc(sizeof(int) * (*colptr));
    memcpy(universe[row], cells[row], sizeof(int) * (*colptr));
  }
  for (int gen = 0; gen < generations; gen++) {
    int** new_universe = calloc(*rowptr + 2, sizeof(int*));
    for (int row = 0; row < *rowptr + 2; row++) {
      new_universe[row] = calloc(*colptr + 2, sizeof(int));
    }
    for (int row = 0; row < *rowptr; row++) {
      memcpy(new_universe[row+1] + 1, universe[row], sizeof(int) * (*colptr));
    }
    free(universe);
    universe = new_universe;
    *rowptr += 2;
    *colptr += 2;
    int neighbors[*rowptr][*colptr];
    for (int row = 0; row < *rowptr; row++) {
      for (int col = 0; col < *colptr; col++) {
        neighbors[row][col] = 0;
        for (int dy = -1; dy <= 1; dy++) {
          for (int dx = -1; dx <= 1; dx++) {
            int neighbor_col = col+dx;
            int neighbor_row = row+dy;
            if (neighbor_col < 0 || neighbor_col >= *colptr) continue;
            if (neighbor_row < 0 || neighbor_row >= *rowptr) continue;
            if (dx == 0 && dy == 0) continue;
            neighbors[row][col] += universe[neighbor_row][neighbor_col];
          }
        }
      }
    }
    for (int row = 0; row < *rowptr; row++) {
      for (int col = 0; col < *colptr; col++) {
        int* current_cell = &universe[row][col];
        if (neighbors[row][col] < 2 || neighbors[row][col] > 3) *current_cell = 0;
        if (neighbors[row][col] == 3) *current_cell = 1;
      }
    }
  }
  int left_bound = *colptr - 1;
  int right_bound = 0;
  int top_bound = *rowptr - 1;
  int bot_bound = 0;
  for (int row = 0; row < *rowptr; row++) {
    for (int col = 0; col < *colptr; col++) {
      if (universe[row][col] == 1) {
        if (col < left_bound) {
          left_bound = col;
        }
        if (col > right_bound) {
          right_bound = col;
        }
        if (row < top_bound) {
          top_bound = row;
        }
        if (row > bot_bound) {
          bot_bound = row;
        }
      }
    }
  }
  int old_rows = *rowptr;
  *colptr = right_bound + 1 - left_bound;
  *rowptr = bot_bound + 1 - top_bound;
  int** out_cells = malloc(sizeof(int*) * (*rowptr));
  for (int row = 0; row < *rowptr; row++) {
    out_cells[row] = malloc(sizeof(int) * (*colptr));
    memcpy(out_cells[row], universe[row + top_bound] + left_bound, sizeof(int) * (*colptr));
  }
  for (int row = 0; row < old_rows; row++) {
    free(universe[row]);
  }
  free(universe);
  return out_cells;
}