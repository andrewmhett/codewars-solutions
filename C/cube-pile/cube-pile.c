#include <stdio.h>

long long findNb(long long m)
{
  long long sum=0;
  long long n=0;
  while (sum<m){
    n++;
    sum+=n*n*n;
  }
  if (sum==m){
    return n;
  }
  return -1;
}