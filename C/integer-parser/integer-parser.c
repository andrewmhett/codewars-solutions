#include <string.h>
#include <stdio.h>
#include <stdlib.h>

long parse_int (const char* number) {
  char* sentence = malloc(strlen(number) + 1);
  strcpy(sentence, number);
  for (int i = 0; i < strlen(sentence); i++) {
    if (sentence[i] == '-') sentence[i] = ' ';
  }
  size_t size = strlen(sentence);
	FILE* sentence_stream = fmemopen(sentence, size, "r");
  int last_magnitude = 10;
  int value = 0;
  int set_aside_value = 0;
  while (1) {
    char* word_buf = NULL;
    size = 0;
    int new_magnitude = 0;
    int new_value = 0;
    if (getdelim(&word_buf, &size, ' ', sentence_stream) == -1) {
      free(word_buf);
      break;
    }
    if (word_buf[strlen(word_buf) - 1] == ' ') word_buf[strlen(word_buf) - 1] = 0;
    if (strcmp(word_buf, "and") == 0) continue;
    if (strcmp(word_buf, "one") == 0)       { new_magnitude = 0; new_value = 1;       }
    if (strcmp(word_buf, "two") == 0)       { new_magnitude = 0; new_value = 2;       }
    if (strcmp(word_buf, "three") == 0)     { new_magnitude = 0; new_value = 3;       }
    if (strcmp(word_buf, "four") == 0)      { new_magnitude = 0; new_value = 4;       }
    if (strcmp(word_buf, "five") == 0)      { new_magnitude = 0; new_value = 5;       }
    if (strcmp(word_buf, "six") == 0)       { new_magnitude = 0; new_value = 6;       }
    if (strcmp(word_buf, "seven") == 0)     { new_magnitude = 0; new_value = 7;       }
    if (strcmp(word_buf, "eight") == 0)     { new_magnitude = 0; new_value = 8;       }
    if (strcmp(word_buf, "nine") == 0)      { new_magnitude = 0; new_value = 9;       }
    if (strcmp(word_buf, "ten") == 0)       { new_magnitude = 1; new_value = 10;      }
    if (strcmp(word_buf, "eleven") == 0)    { new_magnitude = 1; new_value = 11;      }
    if (strcmp(word_buf, "twelve") == 0)    { new_magnitude = 1; new_value = 12;      }
    if (strcmp(word_buf, "thirteen") == 0)  { new_magnitude = 1; new_value = 13;      }
    if (strcmp(word_buf, "fourteen") == 0)  { new_magnitude = 1; new_value = 14;      }
    if (strcmp(word_buf, "fifteen") == 0)   { new_magnitude = 1; new_value = 15;      }
    if (strcmp(word_buf, "sixteen") == 0)   { new_magnitude = 1; new_value = 16;      }
    if (strcmp(word_buf, "seventeen") == 0) { new_magnitude = 1; new_value = 17;      }
    if (strcmp(word_buf, "eighteen") == 0)  { new_magnitude = 1; new_value = 18;      }
    if (strcmp(word_buf, "nineteen") == 0)  { new_magnitude = 1; new_value = 19;      }
    if (strcmp(word_buf, "twenty") == 0)    { new_magnitude = 1; new_value = 20;      }
    if (strcmp(word_buf, "thirty") == 0)    { new_magnitude = 1; new_value = 30;      }
    if (strcmp(word_buf, "forty") == 0)     { new_magnitude = 1; new_value = 40;      }
    if (strcmp(word_buf, "fifty") == 0)     { new_magnitude = 1; new_value = 50;      }
    if (strcmp(word_buf, "sixty") == 0)     { new_magnitude = 1; new_value = 60;      }
    if (strcmp(word_buf, "seventy") == 0)   { new_magnitude = 1; new_value = 70;      }
    if (strcmp(word_buf, "eighty") == 0)    { new_magnitude = 1; new_value = 80;      }
    if (strcmp(word_buf, "ninety") == 0)    { new_magnitude = 1; new_value = 90;      }
    if (strcmp(word_buf, "hundred") == 0)   { new_magnitude = 2; new_value = 100;     }
    if (strcmp(word_buf, "thousand") == 0)  { new_magnitude = 3; new_value = 1000;    }
    if (strcmp(word_buf, "million") == 0)   { new_magnitude = 6; new_value = 1000000; }
    free(word_buf);
    if (new_magnitude < last_magnitude) {
      if (set_aside_value != 0) {
        value += (new_value * set_aside_value);
        set_aside_value = 0;
      } else {
        if (last_magnitude == 3) set_aside_value = new_value;
        else value += new_value;
      }
    } else {
      value *= new_value;
    }
    if (set_aside_value == 0) last_magnitude = new_magnitude;
  }
  if (set_aside_value != 0) value += set_aside_value;
  fclose(sentence_stream);
  return value;
}