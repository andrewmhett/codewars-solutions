#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int* parse(char* program)
{
  int* ret_arr=malloc(strlen(program)*sizeof(int));
  int counter=0;
  int value=0;
  for (int i=0;i<strlen(program);i++){
    switch(program[i]){
        case 'i' : value++; break;
        case 'd' : value--; break;
        case 's' : value*=value; break;
        case 'o' : ret_arr[counter]=value; counter++; break;
    }
  }
  return ret_arr;
}