using System;

public static class Kata
{
  public static string Order(string words)
  {
    string[] word_arr=words.Split(' ');
    string[] out_arr=new string[word_arr.Length];
    for (int i=0;i<word_arr.Length;i++){
      for (int l=0;l<word_arr[i].Length;l++){
        if (!Char.IsLetter(word_arr[i][l])){
          out_arr[word_arr[i][l]-'1']=word_arr[i];
        }
      }
    }
    return String.Join(' ',out_arr);
  }
}