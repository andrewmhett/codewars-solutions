using System;

public class Kata
{
  static bool counted(string str, char character, int index){
    bool output=false;
    for (int i=0;i<index;i++){
      if (str[i] == character){
        output=true;
      }
    }
    return output;
  }
  public static int DuplicateCount(string str)
  {
    int output=0;
    str=str.ToUpper();
    for (int i=0;i<str.Length;i++){
      if ((str.Split(str[i]).Length-1)>1 && !counted(str, str[i],i)){
        output+=1;
      }
    }
    return output;
  }
}