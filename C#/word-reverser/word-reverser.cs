using System.Collections.Generic;
using System.Linq;
using System;

public class Kata
{
  public static string SpinWords(string sentence)
  {
    string[] words=sentence.Split(' ');
    string[] out_words=new string[words.Length];
    int word_count=0;
    for (int i=0;i<words.Length;i++){
      if (words[i].Length>=5){
        string rev_word="";
        for (int p=words[i].Length-1;p>=0;p--){
          rev_word+=words[i][p];
          out_words[word_count]=rev_word;
        }
      }else{
        out_words[word_count]=words[i];
      }
      word_count++;
    }
    return String.Join(' ',out_words);
  }
}