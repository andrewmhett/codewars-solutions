def score(dice):
    print(dice)
    score=0
    for i in range(1,7):
        if dice.count(i)>=3:
            if i == 1:
                score+=1000
            else:
                score+=100*i
            for o in range(3):
                dice.remove(i)
    if dice.count(1)>=1:
        score+=dice.count(1)*100
        dice=[i for i in dice if i != 1]
    if dice.count(5)>=1:
        score+=dice.count(5)*50
    return score