def josephus(items,k):
    delete_index=k-1
    out=[]
    while (len(items)>0):
        while delete_index>=len(items):
            delete_index-=len(items)
        out.append(items[delete_index])
        items=items[0:delete_index]+items[delete_index+1:]
        delete_index+=k-1
    return out