def accum(s):
    counter=0
    str_arr=[]
    for char in s:
        str_arr.append(char.upper()+(char.lower()*counter))
        counter+=1
    return "-".join(str_arr)