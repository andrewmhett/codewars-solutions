def anagrams(word, words):
    orig_dict={}
    out=[]
    for char in word:
        if char not in orig_dict.keys():
            orig_dict[char]=1
        else:
            orig_dict[char]+=1
    for test_word in words:
        test_dict={}
        char_tested=0
        for char in test_word:
            if char not in test_dict.keys():
                test_dict[char]=1
            else:
                test_dict[char]+=1
        for key in test_dict.keys():
            if key in orig_dict.keys():
                if orig_dict[key] == test_dict[key]:
                    char_tested+=1
            else:
                char_tested-=1
        if char_tested == len(orig_dict.keys()):
            out.append(test_word)
    return out