def solution(string,markers):
    out_lines=[]
    for line in string.split("\n"):
        commented=False
        out_line=""
        for char in line:
            if char in markers:
                commented=True
            else:
                if not commented:
                    out_line+=char
        out_lines.append(out_line)
    out_string_array=[]
    for line in out_lines:
        i=len(line)
        end_index=len(line)
        while (i>0):
            i-=1
            if line[i] != " ":
                end_index=i+1
                break
        out_string_array.append(line[0:end_index])
    return "\n".join(out_string_array)