def pig_it(text):
    out_string=""
    out_arr=[]
    for word in text.split(" "):
        if word not in ["!",".","?"]:
            out_arr.append(word[1:]+word[0]+"ay")
        else:
            out_arr.append(word)
    out_string=" ".join(out_arr)
    return out_string