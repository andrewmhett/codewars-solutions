def valid_parentheses(string):
    value_dict={"(":1,")":-1}
    counter=0
    for char in string:
        if char == "(" or char == ")":
            counter+=value_dict[char]
            if counter<0:
                return 0
    return 0 if counter != 0 else 1