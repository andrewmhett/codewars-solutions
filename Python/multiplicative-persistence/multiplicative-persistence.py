def persistence(n):
    counter=0
    while len(str(n))>1:
        counter+=1
        temp_n=1
        for char in str(n):
            temp_n*=int(char)
        n=temp_n
    return counter