def is_valid_walk(walk):
    out=False
    up_down_count=0
    left_right_count=0
    if len(walk) == 10:
        for dir in walk:
            if dir == 'n':
                up_down_count+=1
            if dir == 's':
                up_down_count-=1
            if dir == 'e':
                left_right_count+=1
            if dir == 'w':
                left_right_count-=1
        if up_down_count == 0 and left_right_count == 0:
            out=True
    return out