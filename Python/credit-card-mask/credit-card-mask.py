def maskify(cc):
    retString="#"*(len(cc)-4)
    if (len(retString)) >= 4:
        retString+=cc[len(cc)-4:]
    else:
        retString=cc
    return retString