def high(x):
    scores={}
    for word in x.split(" "):
        scores[word]=0
        for char in word:
            scores[word]+=ord(char)-96
    max=0
    max_word=""
    for word in scores.keys():
        if scores[word]>max:
            max_word=word
            max=scores[word]
    return max_word