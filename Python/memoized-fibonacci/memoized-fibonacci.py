fib_cache=[0,1]

def fibonacci(n):
    if n<len(fib_cache):
        return fib_cache[n]
    else:
        fib_cache.append(fib_cache[len(fib_cache)-2]+fib_cache[len(fib_cache)-1])
    return fibonacci(n - 1) + fibonacci(n - 2)