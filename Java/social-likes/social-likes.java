class Solution {
    public static String whoLikesIt(String... names) {
      String out_str="";
      switch (names.length){
          case 0: out_str="no one likes this"; break;
          case 1: out_str=names[0]+" likes this"; break;
          case 2: out_str=names[0]+" and "+names[1]+" like this"; break;
          case 3: out_str=names[0]+", "+names[1]+" and "+names[2]+" like this"; break;
          default: out_str=names[0]+", "+names[1]+" and "+Integer.toString(names.length-2)+" others like this"; break;
      }
      return out_str;
    }
}