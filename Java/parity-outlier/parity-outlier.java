public class FindOutlier{
  static int find(int[] integers){
    int norm=0;
    for (int i=0;i<2;i++){
      if (integers[i]%2==integers[i+1]%2){
        norm=integers[i]%2;
      }
    }
    int diff=0;
    for (int i=0;i<integers.length;i++){
      if (integers[i]%2 != norm){
        diff=integers[i];
      }
    }
    return diff;
}}