public class Troll {
    static private boolean not_vowel(char c){
      boolean vowel=false;
      char[] vowels={'a','e','i','o','u'};
      for (int i=0;i<5;i++){
        if (c==vowels[i]){
          vowel=true;
        }
      }
      return !vowel;
    }
    public static String disemvowel(String str) {
      String out_str="";
      for (int i=0;i<str.length();i++){
        if (not_vowel(str.toLowerCase().charAt(i))){
          out_str+=str.charAt(i);
        }
      }
      return out_str;
    }
}