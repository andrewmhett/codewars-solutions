function createPhoneNumber(numbers){
  var number_string = "(";
  for (var i = 0; i < 3; i++) {
    number_string += numbers[i];
  }
  number_string += ") ";
  for (var i = 3; i < 6; i++) {
    number_string += numbers[i];
  }
  number_string += '-';
  for (var i = 6; i < 10; i++) {
    number_string += numbers[i];
  }
  return number_string;
}