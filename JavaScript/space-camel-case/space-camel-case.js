// complete the function
function solution(string) {
  var out_string = "";
  for (var i = 0; i < string.length; i++) {
    if (string.charCodeAt(i) >= 65 && string.charCodeAt(i) <= 90) out_string += " ";
    out_string += string[i];
  }
  return out_string;
}