function toWeirdCase(string){
  var out_string = "";
  var word_position = 0;
  for (var i = 0; i < string.length; i++) {
    if (string[i] == " ") {
      word_position = -1;
    }
    if (word_position % 2) {
      out_string += string[i].toLowerCase();
    } else {
      out_string += string[i].toUpperCase()
    }
    word_position++;
  }
  return out_string;
}