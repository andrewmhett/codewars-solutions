function validatePIN (pin) {
  const regex = new RegExp("^(([0-9]{4})|([0-9]{6}))$");
  return regex.test(pin);
}