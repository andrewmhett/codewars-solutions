function towerBuilder(nFloors) {
  var tower = [];
  var width = 1;
  var max_width = (nFloors * 2) - 1;
  var level = "";
  for (var i = 0; i < nFloors; i++) {
    level = " ".repeat((max_width - width) / 2);
    level += "*".repeat(width);
    level += " ".repeat((max_width - width) / 2);
    tower.push(level);
    width+=2;
  }
  return tower;
}