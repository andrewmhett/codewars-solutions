function node_visited(node, visited) {
  var has_been_visited = false;
  for (var i = 0; i < visited.length; i++) {
    if (visited[i] == node) {
      has_been_visited = true;
      break;
    }
  }
  return has_been_visited;
}

function loop_size(node){
  var visited = [];
  var current_node = node;
  while (!node_visited(current_node, visited)) {
    visited.push(current_node);
    current_node = current_node.getNext();
  }
  var loop_size = 0;
  for (var i = 0; i < visited.length; i++) {
    if (visited[i] == current_node) {
      loop_size = visited.length - i;
      break;
    }
  }
  return loop_size;
}