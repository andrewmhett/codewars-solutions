function findOdd(A) {
  var frequency_map = {};
  for (var i = 0; i < A.length; i++) {
    if (frequency_map[A[i]] == null) frequency_map[A[i]] = 0;
    frequency_map[A[i]]++;
  }
  var odd_number = 0;
  for (var i = 0; i < A.length; i++) {
    if (frequency_map[A[i]] % 2) {
      odd_number = A[i];
      break;
    }
  }
  return odd_number;
}