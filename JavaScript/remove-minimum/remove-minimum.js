function removeSmallest(numbers) {
  var not_dull = [];
  var min = Number.MAX_VALUE;
  for (var i = 0; i < numbers.length; i++) {
    if (numbers[i] < min) min = numbers[i];
  }
  for (var i = 0; i < numbers.length; i++) {
    if (numbers[i] != min) {
      not_dull.push(numbers[i]);
    } else {
      min = -1;
    }
  }
  return not_dull;
}