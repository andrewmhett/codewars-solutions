function expandedForm(num) {
  var num_string = num.toString();
  var out_string = "";
  for (var i = 0; i < num_string.length; i++) {
    if (num_string[i] == "0") continue;
    out_string += num_string[i] + "0".repeat(num_string.length - i - 1);
    if (num % Math.pow(10, num_string.length - i - 1) == 0) break;
    if (i < num_string.length - 1) out_string += " + ";
  }
  return out_string;
}