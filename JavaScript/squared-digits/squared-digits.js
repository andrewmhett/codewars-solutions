function squareDigits(num){
  var out_string = "";
  var in_string = num.toString();
  for (var i = 0; i < in_string.length; i++) {
    out_string += Math.pow(parseInt(in_string[i]), 2).toString();
  }
  return parseInt(out_string);
}