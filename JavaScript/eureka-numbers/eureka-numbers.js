function sumDigPow(a,b) {
  var num_list = [];
  for (var i = a; i <= b; i++) {
    var num_string = i.toString();
    var sum = 0;
    for (var power = 1; power <= num_string.length; power++) {
      sum += Math.pow(num_string[power-1], power);
    }
    if (sum == i) num_list.push(i);
  }
  return num_list;
}