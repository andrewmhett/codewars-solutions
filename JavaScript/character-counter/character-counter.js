function count (string) {  
  var frequency_map = {};
  for (var i = 0; i < string.length; i++) {
    if (frequency_map[string[i]] == null) frequency_map[string[i]] = 0;
    frequency_map[string[i]]++;
  }
  return frequency_map;
}