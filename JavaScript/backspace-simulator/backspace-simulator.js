function cleanString(s) {
  var out_array = [];
  var backspace_count = 0;
  for (var i = s.length - 1; i >= 0; i--) {
    if (s[i] == "#") {
      backspace_count++;
    } else if (backspace_count > 0) {
      backspace_count--;
    } else {
      out_array.push(s[i]);
    }
  }
  out_array.reverse();
  return out_array.join("");
}