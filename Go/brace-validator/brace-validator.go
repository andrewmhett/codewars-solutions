package kata

func ValidBraces(str string) bool {
  pair_array:=[3]int{};
  valueMap:=map[byte][2]int{
    '(':[2]int{0,1},
    ')':[2]int{0,-1},
    '[':[2]int{1,1},
    ']':[2]int{1,-1},
    '{':[2]int{2,1},
    '}':[2]int{2,-1},
  }
  last_index:=0;
  last_value:=0;
  for i:=0;i<len(str);i++{
    if str[i]==')' && pair_array[0]==0{
      return false;
    }
    if str[i]==']' && pair_array[1]==0{
      return false;
    }
    if str[i]=='}' && pair_array[2]==0{
      return false;
    }
    if last_index != valueMap[str[i]][0] && last_value > valueMap[str[i]][1]{
      return false;
    }
    last_index=valueMap[str[i]][0];
    last_value=valueMap[str[i]][1];
    pair_array[valueMap[str[i]][0]]+=valueMap[str[i]][1];
  }
  for i:=0;i<len(pair_array);i++{
    if pair_array[i] != 0{
      return false;
    }
  }
  return true;
}