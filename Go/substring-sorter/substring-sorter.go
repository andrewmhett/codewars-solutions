package kata

import("strings");

func InArray(array1 []string, array2 []string) []string {
  var array3 []string;
  for i:=0;i<len(array1);i++{
    for o:=0;o<len(array2);o++{
      if strings.Contains(array2[o],array1[i]){
        var dup=false;
        for c:=0;c<len(array3);c++{
          if array1[i] == array3[c]{
            dup=true;
          }
        }
        if !dup{
          array3=append(array3,array1[i]);
        }
      }
    }
  }
  var sorted=false;
  for !sorted{
    var temp_sorted=true;
    for i:=0;i<len(array3)-1;i++{
      if int(array3[i][0])>int(array3[i+1][0]){
        temp_sorted=false;
        var swp=array3[i+1];
        array3[i+1]=array3[i];
        array3[i]=swp;
      }else if int(array3[i][0])==int(array3[i+1][0]){
        if int(array3[i][1])>int(array3[i+1][1]){
          temp_sorted=false;
          var swp=array3[i+1];
          array3[i+1]=array3[i];
          array3[i]=swp;
        }
      }
    }
    sorted=temp_sorted;
  }
  if array3==nil{
    return make([]string,0);
  }
  return array3;
}