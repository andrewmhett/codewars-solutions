package multiplicationtable

func MultiplicationTable(size int) [][]int {
  out:=[][]int{};
  for i:=0;i<size;i++{
    row:=make([]int,size);
    out=append(out,row);
  }
  for x:=0;x<size;x++{
    for y:=0;y<size;y++{
      out[x][y]=(x+1)*(y+1);
    }
  }
  return out;
}