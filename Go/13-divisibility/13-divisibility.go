package kata

import("strconv")

func Thirt(n int) int {
  str:=strconv.Itoa(n);
  sequence:=[6]int{1,10,9,12,3,4};
  counter:=0;
  sum:=n;
  prev_sum:=0;
  for sum!=prev_sum{
    str=strconv.Itoa(sum);
    prev_sum=sum;
    sum=0;
    counter=0;
    for i:=len(str)-1;i>=0;i--{
      if counter>=len(sequence){
        counter=0;
      }
      sum+=int(str[i]-'0')*sequence[counter]
      counter++;
    }
  }
  return sum;
}