package kata

func Evaporator(content float64, evapPerDay int, threshold int) int {
  day:=0;
  percent:=100.0;
  for int(percent)>=threshold{
    percent-=percent*(float64(evapPerDay)/100);
    day++;
  }
  return day;
}