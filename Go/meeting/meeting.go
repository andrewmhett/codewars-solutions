package kata

import("strings");

func Meeting(s string) string {
  name_arr:=strings.Split(strings.ToUpper(s),";");
  for i:=0;i<len(name_arr);i++{
    name_arr[i]="("+strings.Split(name_arr[i],":")[1]+", "+strings.Split(name_arr[i],":")[0]+")";
  }
  sorted:=false;
  for !sorted{
    swapped:=false;
    for i:=0;i<len(name_arr)-1;i++{
      if strings.Compare(name_arr[i],name_arr[i+1])==1{
        swp:=name_arr[i+1];
        name_arr[i+1]=name_arr[i];
        name_arr[i]=swp;
        swapped=true;
      }
    }
    if !swapped{
      sorted=true;
    }
  }
  return strings.Join(name_arr,"");
}