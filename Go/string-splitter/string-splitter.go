package kata

func Solution(str string) []string {
    var out []string;
    sub := "";
    for i:=0;i<len(str);i++{
        sub+=string([]byte{str[i]});
        if len(sub)==2{
            out=append(out,sub);
            sub="";
        }else if i == len(str)-1{
            out=append(out,sub+string([]byte{'_'}));
        }
    }
    return out;
}