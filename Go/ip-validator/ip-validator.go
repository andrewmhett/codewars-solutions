package kata

import("strings");
import("strconv");

func Is_valid_ip(ip string) bool {
  s_arr:=strings.Split(ip,".");
  if len(s_arr)==4{
    valid:=true;
    for _, octet:=range s_arr{
      if len(octet)>1{
        if octet[0] == '0'{
          valid=false;
        }
      }
      i,err:=strconv.Atoi(octet);
      if i>255 || i<0 || err != nil{
        valid=false;
      }
    }
    return valid;
  }
  return false;
}