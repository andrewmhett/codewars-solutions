package kata

func TwoSum(numbers []int, target int) [2]int {
  var out_arr [2]int;
  end:=false;
  for i1:=0;i1<len(numbers);i1++{
    for i2:=0;i2<len(numbers);i2++{
      if i2 != i1{
        if numbers[i1]+numbers[i2]==target{
          out_arr[0]=i1;
          out_arr[1]=i2;
          end=true;
          break;
        }
      }
    }
    if end{
      break;
    }
  }
  if (out_arr[1]<out_arr[0]){
    var swp=out_arr[0];
    out_arr[0]=out_arr[1];
    out_arr[1]=swp;
  }
  return out_arr;
}