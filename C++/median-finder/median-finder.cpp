#include <vector>

using namespace std;

int find_even_index (const vector <int> numbers) {
  int sum1, sum2;
  for (int i=0;i<(int)numbers.size();i++){
    sum1=0;
    sum2=0;
    for (vector<int>::const_iterator it = numbers.begin(); it != numbers.begin()+i; ++it){
      sum1+=*it;
    }
    for (vector<int>::const_iterator it = numbers.end()-1; it != numbers.begin()+i; --it){
      sum2+=*it;
    }
    if (sum1==sum2){
      return i;
    }
  }
  return -1;
}