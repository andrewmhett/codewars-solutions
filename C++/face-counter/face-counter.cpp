bool is_smiling(std::string face){
    bool eyes=false;
    bool nose=false;
    bool mouth=false;
    if (face[0]==':' || face[0]==';'){
        eyes=true;
    }
    if (face.length() == 3){
        if (face[1]=='-' || face[1]=='~'){
            nose=true;
        }
    }else{
        nose=true;
    }
    if (face[face.length()-1]==')' || face[face.length()-1]=='D'){
        mouth=true;
    }
    return eyes && nose && mouth;
}

int countSmileys(std::vector<std::string> arr) {
    int counter=0;
    for (std::vector<std::string>::iterator it = arr.begin(); it != arr.end(); ++it){
    counter+=is_smiling(*it);
    std::cout << *it << std::endl;
    }
    return counter;
}