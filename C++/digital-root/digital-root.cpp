int digital_root(int n)
{
    while (std::to_string(n).length()>1){
        std::string str = std::to_string(n);
        n=0;
        for (int i=0;i<str.length();i++){
            std::string s(1,str[i]);
            n+=std::stoi(s,NULL);
        }
    }
    return n;
}