using namespace std;

map<int,string> numeralMap = {
  {1,"I"},
  {4,"IV"},
  {5,"V"},
  {9,"IX"},
  {10,"X"},
  {40,"XL"},
  {45,"XLV"},
  {50,"L"},
  {90,"XC"},
  {100,"C"},
  {400,"CD"},
  {500,"D"},
  {900,"CM"},
  {1000,"M"}
};

string solution(int number){
  int keys[14] = {1,4,5,9,10,40,45,50,90,100,400,500,900,1000};
  int index=13;
  string output="";
  cout << "-----" << endl;
  while (number != 0){
    while ((number/keys[index])>=1){
      cout << to_string(number)+"-"+to_string(keys[index]) << endl;
      number-=keys[index];
      output+=numeralMap[keys[index]];
    }
    index--;
  }
  return output;
}