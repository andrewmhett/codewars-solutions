#include <string>

using namespace std;

class looper {
  private:
    string in_str;
    size_t curr_index;
  public:
    looper(string in_str) {
      this->in_str = in_str;
      this->curr_index = 0;
    }
    char operator()() {
      if (curr_index == in_str.length()) curr_index = 0;
      return in_str[curr_index++];
    }
};

auto makeLooper(const std::string& str)
{
  return looper(str);
}
