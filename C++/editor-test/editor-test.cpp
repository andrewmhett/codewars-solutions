#include <string>
#include <vector>

std::vector<std::string> number(const std::vector<std::string> &lines)
{
  int count=0;
  std::vector<std::string> output;
  for (std::vector<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it){
    count++;
    output.push_back(to_string(count)+": "+*it);
  }
  return output;
}