using namespace std;
#include <limits>

class ToSmallest
{
public:
    static std::vector<long long> smallest(long long n){
      string str=to_string(n);
      string result_str;
      vector<long long> results;
      vector<long long> i_vec;
      vector<long long> j_vec;
      for (int i=0;i<(int)str.length();i++){
        for (int j=0;j<(int)str.length();j++){
          result_str=str;
          string swp(1,str[i]);
          result_str.erase(result_str.begin()+i);
          result_str.insert(j,swp);
          results.push_back(stoll(result_str,NULL));
          i_vec.push_back(i);
          j_vec.push_back(j);
        }
      }
      long long min = numeric_limits<long long>::max();
      vector<long long> min_result;
      int counter=0;
      int min_index=0;
      for (vector<long long>::iterator it = results.begin(); it != results.end(); ++it){
        if (*it<min){
          min=*it;
          min_index=counter;
        }
        counter++;
      }
      min_result.push_back(min);
      min_result.push_back(*(i_vec.begin()+min_index));
      min_result.push_back(*(j_vec.begin()+min_index));
      return min_result;
    }
};