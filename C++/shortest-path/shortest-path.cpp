#include <string>
#include <vector>
#include <queue>
#include <cmath>

using namespace std;

struct Coord {
    int x;
    int y;
};

int path_finder(string maze) {
    const int dimension = ((int)sqrt(4 * maze.size() + 5) - 1)/2;
    vector<vector<int> > visited(dimension, vector<int>(dimension, -1));
    
    int i = 0, j = 0; // row, col
    for (auto& c : maze) {
        switch (c) {
          case 'W' : visited[i][j] = -2;
          case '.' : ++j; break;
          case '\n': ++i; j = 0;
        }
    }
    visited[0][0] = 0;
    queue<Coord> walking;
    walking.push( {0,0} );
    
    const int drow[] = {0,  1,  0, -1};
    const int dcol[] = {1,  0, -1,  0};
    
    while (!walking.empty()) {
        i = walking.front().x;
        j = walking.front().y;
        walking.pop();
        
        for (int k = 0; k < 4; ++k) {
            int row = i + drow[k];
            int col = j + dcol[k];
            
            if (row >= 0 && col >= 0 && row < dimension && col < dimension && visited[row][col] == -1) {
                visited[row][col] = visited[i][j] + 1;
                walking.push( {row, col} );
            }
        }
    }
    
    return visited[dimension-1][dimension-1];
}