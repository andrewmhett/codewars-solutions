#include <iostream>
#include <string>
#include <vector>

using namespace std;

void permute(string s, int n, vector<string>& perms){
  if (n<(int)s.length()){
    for (int i=n;i<(int)s.length();i++){
      string out=s;
      char swp=out[i];
      out[i]=out[n];
      out[n]=swp;
      if (!count(perms.begin(),perms.end(),out)){
        perms.push_back(out);
      }
      permute(out, n+1, perms);
    }
    permute(s, n+1, perms);
  }
}

vector<string> permutations(string s) {
  vector<string> perms;
  perms.clear();
  perms.push_back(s);
  permute(s, 0, perms);
  vector<string> out;
  return perms;
}