class Ball
{
    public: static int maxBall(int v0){
      float mts=((float)v0/3600)*1000;
      float time=1.0;
      float prev=0;
      while (prev<(mts*time)-(.981*time*time*0.5)){
        prev=(mts*time)-(.981*time*time*0.5);
        time+=1.0;
      }
      return time-1;
    }
};