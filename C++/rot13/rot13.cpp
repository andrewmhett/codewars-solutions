#include <string>
using namespace std;

string rot13(string msg)
{
  for (int i=0;i<(int)msg.length();i++){
    if (((int)msg[i]>=65 && (int)msg[i]<=90) || ((int)msg[i]>=97 && (int)msg[i]<=122)){
      int num=((int)msg[i]+13);
      if (num-13<=90 && num>90){
        num-=26;
      }
      if (num-13>=97 && num>122){
        num-=26;
      }
      msg[i]=(char)num;
    }
  }
  return msg;
}