std::vector<int> tribonacci(std::vector<int> signature, int n)
{
    int calc_arr[n];
    std::vector<int> result;
    if (n<signature.size()) {
      for (int i=0;i<n;i++) {
        result.push_back(*(signature.begin()+i));
      }
    } else {
      for (std::vector<int>::iterator it = signature.begin(); it != signature.end(); ++it){
        result.push_back(*it);
      }
      for (int i=signature.size();i<n;i++) {
        int sum=0;
        for (int i=3;i>0;i--){
          sum+=*(result.end()-i);
        }
        result.push_back(sum);
      }
    }
    return result;
}