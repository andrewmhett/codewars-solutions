#include <string>

using namespace std;

string to_camel_case(string text) {
  if (text.length()>0){
    while (text.find("-")<text.length() || text.find("_")<text.length()){
      if (text.find("-")<text.length()){
        text[text.find("-")+1]=toupper(text[text.find("-")+1]);
        text.erase(text.begin()+text.find("-"));
      }
      if (text.find("_")<text.length()){
        text[text.find("_")+1]=toupper(text[text.find("_")+1]);
        text.erase(text.begin()+text.find("_"));
      }
    } 
    return text;
  }else{
    return "";
  }
}