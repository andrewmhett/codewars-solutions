#include <math.h>

using namespace std;

class DigPow {
public:
  static int digPow(int n, int p){
    string str = to_string(n);
    int sum=0;
    for (int i=0;i<str.length();i++){
      string s(1,str[i]);
      sum+=pow(stoi(s,NULL),p+i);
      cout << sum << endl;
    }
    cout << "-----" << endl;
    cout << n << endl;
    int k=0;
    while (n*k<sum){
      k++;
    }
    if (n*k==sum){
      return k;
    }
    return -1;
  }
};