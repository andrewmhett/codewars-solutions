using std::vector;
typedef unsigned long long ull;

int prev[2]={0,1};
class ProdFib
{
public:
  static ull get_fib_prod(int n){
    prev[0]=0;
    prev[1]=1;
    for (int i=2;i<=n;i++){
      int next_fib=prev[0]+prev[1];
      prev[0]=prev[1];
      prev[1]=next_fib;
    }
    return (ull)prev[0]*(ull)prev[1];
  }
  static vector<ull> productFib(ull prod){
    ull test_prod=0;
    int i=2;
    vector<ull> ret_vec;
    while (test_prod<prod){
        test_prod=get_fib_prod(i);
        i++;
    }
    ret_vec.push_back(prev[0]);
    ret_vec.push_back(prev[1]);
    if (test_prod>prod){
      ret_vec.push_back(false);
    }else{
      ret_vec.push_back(true);
    }
    return ret_vec;
  }
};