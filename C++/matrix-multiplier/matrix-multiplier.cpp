using namespace std;

std::vector<std::vector<int>> matrix_multiplication(std::vector<std::vector<int>> &a, std::vector<std::vector<int>> &b, size_t n){
  vector<vector<int>> mat_c;
  mat_c.resize(n);
  for (int i=0;i<n;i++){
    mat_c[i].resize(n);
  }
  for (int y=0;y<n;y++){
    for (int x=0;x<n;x++){
      int sum=0;
      for (int i=0;i<n;i++){
        sum+=a[y][i]*b[i][x];
      }
      mat_c[y][x]=sum;
    }
  }
  return mat_c;
}