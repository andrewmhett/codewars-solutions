#include <math.h>

bool narcissistic( int value ){
  std::string str = std::to_string(value);
  std::string::size_type temp_int;
  int check_int = 0;
  for (int i=0;i<str.length();i++){
    std::string s(1,str[i]);
    int temp = std::stoi(s,&temp_int);
    std::cout << temp_int << std::endl;
    check_int+=pow(temp,str.length());
  }
  std::cout << value;
  return value == check_int;
}