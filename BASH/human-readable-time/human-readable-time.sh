#!/bin/bash

seconds=$1
hour=`echo -n $(( $seconds / 3600 ))`
minute=`echo -n $(( $(( $seconds % 3600 )) / 60 ))`
second=`echo -n $(( $seconds % 60 ))`
if (( `echo -n $hour | wc -c` == 1)); then
  echo -n "0"
fi
echo -n $hour
echo -n ":"
if (( `echo -n $minute | wc -c` == 1)); then
  echo -n "0"
fi
echo -n $minute
echo -n ":"
if (( `echo -n $second | wc -c` == 1)); then
  echo -n "0"
fi
echo -n $second
