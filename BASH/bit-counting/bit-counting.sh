#!/bin/bash
n=$1
counter=0
bin=$(echo "obase=2;$n" | bc)
for i in $(seq 1 ${#bin}); do
    let counter+=$(( ${bin:i-1:1} == 1 ))
done

echo $counter