#!/bin/bash
f=$1;
p=$2;
c=$3;
n=$4;
i=$5;
count=0;

while [[ $f -gt 0 ]] || [[ $f == 0 ]]; do
  ((count+=1))
  f=$(echo "$f+($p*$f/100)-$c" | bc)
  c=$(echo "$c+($c*$i/100)" | bc)
done

if (("$count >= $n")); then
  echo true
else
  echo false
fi