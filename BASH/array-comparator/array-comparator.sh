#parameters are strings; return "true" or "false" (see Sample Tests)
#!/bin/bash
comp() {
    a=$1
    b=$2
    c=$b
    if [[ ${#a[@]} -gt 0 ]] && [[ ${#b[@]} -gt 0 ]]; then
      check_list=()
      out=0
      for num in ${a[@]}; do
        check_list+=($(bc <<< "$num*$num"))
      done
      checked_indicies=()
      for square in ${check_list[@]}; do
          valid=0
          index=0
          for check_square in ${b[@]}; do
              if [[ $square -eq $check_square ]]; then
                  index_checked=true
                  for i in ${checked_indicies[@]}; do
                    if (( $i == $index )); then
                      index_checked=false
                    fi
                  done
                  if $index_checked; then
                      valid=1
                      checked_indicies+=($index)
                      break
                  fi
              fi
              (( index++ ))
          done
          (( out+=valid ))
      done
      if [[ $out -eq ${#check_list[@]} ]]; then
            echo true
            exit
      fi
    fi
    echo false
}
comp "$1" "$2"