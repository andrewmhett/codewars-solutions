letters=$1

total=96

if [[ ${#letters} -gt 0 ]]; then
  for i in $(seq 1 ${#letters}); do
    (( total+=($(printf '%d\n' "'${letters:i-1:1}")-96) ))
  done

  while [[ $total -gt 122 ]]; do
    (( total-=26 ))
  done
  printf "\x$(printf %x $total)"
else
  echo z
fi